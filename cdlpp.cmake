function(use_cdlpp target cdlpp_src_dir)
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(cdlpp IMPORTED_TARGET cdlpp)

    option(FORCE_SUBMODULE_CDLPP "Use CDLPP from submodule even if installed in system" No)
    if(cdlpp_FOUND AND NOT FORCE_SUBMODULE_CDLPP)
        set(CDLPP_LIB PkgConfig::cdlpp)
    else()
        execute_process(
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            COMMAND git submodule update --init --recursive --depth 1
        )
        add_subdirectory(${cdlpp_src_dir})
        set(CDLPP_LIB "cdlpp")
    endif()

    target_link_libraries(${PROJECT_NAME} PUBLIC
            ${CDLPP_LIB})
endfunction()
