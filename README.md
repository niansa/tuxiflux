# Tuxiflux
A feature-filled free and open source Discord bot based on CDL++, Discord++ and Boost ASIO!

This bot is the first big bot written on CDL++, so in case you want to learn how to use that framework... Come on it's an open source project, just read the source code and learn!

## Default prefix
The default prefix is `t#`

## Commands
For a list of commands, this out `help/help_1.txt`!

## Setup
 1. Install [CDL++](https://gitlab.com/niansa/cdlpp)
 2. Copy `config.json.example` to `config.json` and fill it
 3. Clone submodules: `git submodule update --init --depth 1 --recursive`
 4. Make and chdir into build directory: `mkdir build && cd build`
 5. Configure the project: `cmake ..`
 6. Get everything compiled: `make -j$(nproc)`
 7. Run the bot: `./tuxiflux`
 8. Enjoy!

Hint: the database does not need any special setup. I can recommend [ElephantSQL](https://elephantsql.com) for simple setups.

## Living example
Here's a running instance of Tuxifan ready for daily use: [Discord Bot Invite](https://discordapp.com/api/oauth2/authorize?client_id=788310535799308288&permissions=8&scope=applications.commands%20bot)
