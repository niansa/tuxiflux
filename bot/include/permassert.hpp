#pragma once
#define permassert(msg, channel, permission) if (not channel->has_perm(msg->author, permission)) {channel->send(":warning: You need the permission **"#permission"** to do that"); return;}0
#define teamassert(msg) {bool cont = false; for (const auto& [_, user_id] : env.settings["team"].items()) {if (user_id == msg->author->id) {cont = true; break;}} if (not cont) {return;}}0
#define gchannelassert(channel) if (channel->type == ChannelTypes::DM) {channel->send(":warning: How am I supposed to do that in a DM!?"); return;}0
