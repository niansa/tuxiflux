#pragma once
#include <cdlpp/bot.hpp>
#include <cdlpp/cdltypes-incomplete.hpp>

void send_badusage(CDL::CChannel channel, const std::string& command);
