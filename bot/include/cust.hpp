#pragma once
#include <string>
#include <unordered_map>
#include <cdlpp/bot.hpp>
#include <cdlpp/cdltypes-incomplete.hpp>
#include "database.hpp"


extern const std::string default_prefix;
extern std::map<uint64_t, std::string> prefix_cache;

void in_main();
void on_message(CDL::CMessage msg, std::function<void (CDL::CMessage)> cb);
void on_error(const std::string& message);
void get_prefix(CDL::CChannel channel, std::function<void (const std::string&)> cb);
bool is_globalchat(CDL::CChannel channel);


constexpr uint32_t color_ok = 0x00ff00,
                   color_err = 0xff0000,
                   color_warn = 0xff6600;

inline nlohmann::json simpleEmbed(const std::string& msg, uint32_t color = color_ok) {
    return {{"description", msg}, {"color", color}};
}
