#pragma once

namespace db_templates {
    enum type {
        guild,
        user,
        member,
        _end [[maybe_unused]], // Must be last one before _default
        _default = guild // Must be last one
    };
}
