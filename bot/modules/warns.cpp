#include <vector>
#include <memory>
#include <cdlpp/cdltypes.hpp>
#include "help.hpp"
#include "generic_msgs.h"
#include "permassert.hpp"
using namespace std;
using namespace CDL;



class Warns {
    inline static string get_db_identifier(uint64_t user_id, uint64_t guild_id) {
        return to_dbid(user_id)+' '+to_dbid(guild_id);
    }

    static void warns(CMessage msg, CChannel channel, cmdargs& args) {
        auto guild = channel->get_guild();
        // Get target member
        uint64_t target_user;
        if (args.size() == 1) {
            target_user = extras::find_user_id(guild, args[0]);
            // Check if member was found
            if (not target_user) {
                channel->send(NO_SUCH_USER);
                return;
            }
        } else {
            target_user = msg->author->id;
        }
        auto identifier = get_db_identifier(target_user, guild->id);
        // Get amount of warnings
        env.db->get<int>(identifier, "WARNS", [=] (int warns_a) {
            // Check if member has any warnings
            if (warns_a) {
                // Create list of warnings in heap
                auto warnings = make_shared<vector<string>>();
                warnings->reserve(warns_a);
                // Copy amount of warnings to heap
                auto hwarns = make_shared<int>(warns_a);
                // Define functionm that shows the result
                auto show_warns = [=] () {
                    // Get user
                    fetch::user(target_user, [=] (CUser target) {
                        // Check if user could be found
                        if (not target_user) {
                            channel->send(NO_SUCH_USER);
                            return;
                        }
                        // Generate string
                        ostringstream text;
                        for (const auto& reason : *warnings) {
                            text << " - " << reason << '\n';
                        }
                        // Send as embed
                        channel->send_embed({
                                                {"title", "Warnings to "+target->get_full_name()},
                                                {"description", text.str()}
                                            });
                    });
                };
                // Define reader for next warning
                auto next_warn = make_shared<function<void ()>>();
                *next_warn = [=] () {
                    if ((*hwarns)--) {
                        env.db->get<string>(identifier, "WARN"+to_string(*hwarns), [=] (auto warnstr) {
                            warnings->push_back(warnstr);
                            (*next_warn)();
                        }, db_templates::member);
                    } else {
                        // Show warnings
                        show_warns();
                    }
                };
                (*next_warn)();
            } else {
                channel->send_embed(simpleEmbed(":warning: This user has no warnings!", color_warn));
            }
        }, db_templates::member);
    }

    static void warn(CMessage msg, CChannel channel, cmdargs& args) {
        auto guild = channel->get_guild();
        // Check permissions
        permassert(msg, channel, Permissions::BAN_MEMBERS);
        // Check arguments
        if (args.size() != 2) {
            send_badusage(channel, "warn");
            return;
        }
        // Check the mentioned user
        auto target_user = extras::find_user_id(guild, args[0]);
        // Check if user was found
        if (not target_user) {
            channel->send(NO_SUCH_USER);
            return;
        }
        // Check how many time the user has been warned already
        auto identifier = get_db_identifier(target_user, guild->id);
        env.db->get<int>(identifier, "WARNS", [=] (auto warns_a) {
            // Increase amount of warns, add warning and check if 3 warnings were reached
            if (++warns_a <= 3) {
                // Set reason
                env.db->update(identifier, "WARN"+to_string(warns_a-1), args[1], [=] (const bool) {
                    // Update counter
                    env.db->update(identifier, "WARNS", warns_a, [=] (const bool) {
                        // Return list of warnings
                        cmdargs fakeargs = {to_string(target_user)};
                        warns(nullptr, channel, fakeargs);
                        // Ban the member if required
                        if (warns_a == 3) {
                            guild->ban(target_user, "3 warnings");
                            channel->send(args[0]+" was **banned**!");
                        }
                    }, db_templates::member);
                }, db_templates::member);
            } else {
                channel->send_embed(simpleEmbed(":warning: The user already has 3 warnings!", color_err));
            }
        }, db_templates::member);
    }

    static void unwarn(CMessage msg, CChannel channel, cmdargs& args) {
        auto guild = channel->get_guild();
        // Check permissions
        permassert(msg, channel, Permissions::BAN_MEMBERS);
        // Check arguments
        if (args.size() != 1) {
            send_badusage(channel, "unwarn");
            return;
        }
        // Check the mentioned user
        auto target_user = extras::find_user_id(guild, args[0]);
        // Check if user was found
        if (not target_user) {
            channel->send(NO_SUCH_USER);
            return;
        }
        // Decrease warn counter
        env.db->update(get_db_identifier(target_user, guild->id), "WARNS = WARNS - 1", [=] (const bool error) {
            if (not error) {
                channel->send_embed(simpleEmbed("This members last warning was removed"));
            } else {
                channel->send_embed(simpleEmbed(":warning: This member does not yet have any warnings", color_err));
            }
        }, db_templates::member);
    }


public:
    Warns() {
        register_command("warns", warns, 1);
        register_command("warn", warn, 2);
        register_command("unwarn", unwarn, 1);

        intents::guild_ban_remove.push_back([] (CGuild server, const Ban& ban) {
            // Reset warnings for the unbanned user
            env.db->update(get_db_identifier(ban.user->id, server->id), "WARNS", 0, nullptr, db_templates::member);
        });
    }
};



static Warns warns;
