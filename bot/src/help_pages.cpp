#include <string>
#include <unordered_map>
#include "help_pages.hpp"

#include "embed_help_1.h"
#include "embed_help_notfound.h"
#include "embed_help_syntaxtmpl.h"
#include "embed_syntaxes.h"

const std::unordered_map<std::string, std::string> help_files = {
    {"help_1", std::string(reinterpret_cast<char*>(help_1_txt), help_1_txt_len)},
    {"help_notfound", std::string(reinterpret_cast<char*>(help_notfound_txt), help_notfound_txt_len)},
    {"help_syntaxtmpl", std::string(reinterpret_cast<char*>(help_syntaxtmpl_txt), help_syntaxtmpl_txt_len)},
    {"syntaxes", std::string(reinterpret_cast<char*>(syntaxes_json), syntaxes_json_len)}
};
